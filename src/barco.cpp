#include <iostream>
#include <string> 

#include "./../obj/barco.hpp"
#include "./../obj/submarino.hpp"

using namespace std; 

Barco::Barco(){
    orientacao = "Nenhum";
    tipo_de_barco = "nenhum";
	tamanho = 0; 
	localizacao_x = 0;
	localizacao_y = 0;
	vida_total = 0;
	vida_atual = 0; 
}

Barco::~Barco(){}

string Barco::get_orientacao(){
	return orientacao; 
}	
void Barco::set_orientacao(string orientacao){
	this-> orientacao = orientacao; 
}	

string Barco::get_tipo_de_barco(){
	return tipo_de_barco; 
}	
void Barco::set_tipo_de_barco(string tipo_de_barco){
	this-> tipo_de_barco = tipo_de_barco; 
}

int Barco::get_tamanho(){
	return tamanho; 
}
void Barco::set_tamanho(int tamanho){
	this -> tamanho = tamanho; 
}	
    
int Barco::get_localizacao_x(){
	return localizacao_x = localizacao_x; 
}
void Barco::set_localizacao_x(int localizacao_x){
	this -> localizacao_x = localizacao_x; 
}
	    
int Barco::get_localizacao_y(){
	return localizacao_y; 
}
void Barco::set_localizacao_y(int localizacao_y){
	this -> localizacao_y = localizacao_y; 
}
		    
int Barco::get_vida_total(){
	return vida_total; 
}
void Barco::set_vida_total(int vida_total){
	this -> vida_total = vida_total; 
}
int Barco::get_vida_atual(){
	return vida_atual; 
}

int Barco::posicoes(int, int , char**){
    return 0; 
}

//int Barco::posicoes(int, int ,int, char**){
//   return 0; 
//}

void Barco::set_vida_atual(int vida_atual){
	this -> vida_atual = vida_atual; 
}

int Barco::habilidade_especial(){
	
	return 0; 
}


