#include <iostream> 
#include <string> 
#include "./../obj/barco.hpp"
#include "./../obj/submarino.hpp"

using namespace std; 

Submarino::Submarino(){ 
}
Submarino::~Submarino(){} 

int Submarino::posicoes(int p_y, int p_x,char *sentido,char **MAPA_CH)
{
    MAPA_CH[p_y][p_x] = 'S';
    if(sentido[0] == 'c' )MAPA_CH[p_y-1][p_x] = 'S';
    
    if(sentido[0] == 'b' )MAPA_CH[p_y+1][p_x] = 'S';            
    
    if(sentido[0] == 'e' )MAPA_CH[p_y][p_x-1] = 'S';
    
    if(sentido[0] == 'd' )MAPA_CH[p_y][p_x+1] = 'S';
}

int Submarino::vida(int p_y, int p_x,char *sentido,int **MAPA_INT){
    MAPA_INT[p_y][p_x] = 2;
    if(sentido[0] == 'c' )MAPA_INT[p_y-1][p_x] = 2;

    if(sentido[0] == 'b' )MAPA_INT[p_y+1][p_x] = 2;            

    if(sentido[0] == 'e' )MAPA_INT[p_y][p_x-1] = 2;

    if(sentido[0] == 'd' )MAPA_INT[p_y][p_x+1] = 2;
}
