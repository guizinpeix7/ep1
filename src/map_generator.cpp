#include <iostream>
#include <string> 
#include "./../obj/barco.hpp"
#include "./../obj/submarino.hpp"
#include "./../obj/canoa.hpp"
#include "./../obj/porta_avioes.hpp"
#include "./../obj/map_generator.hpp"
#include <stdlib.h>
#include <time.h>

//Fazer com que os dados aqui escritos sejam passados para o arquivo txt do mapa (:' 
//A sacada para esta ideia dar certo sera linkar a posicao da linha e coluna da minha matriz de caracteres com a linha e coluna da matriz de inteiros que ira rodar o jogo. 
//PREOLAA 

//MEU PRIMEIRO PROBLEMA ESTA AQUI o.O
using namespace std; 

Map_generator::Map_generator(){
	n_lines = 0; 
	n_rows = 0; 
	line_indicator = 0; 
	row_indicator = 0;
	printf("Criando mapa!....\n"); 
}
Map_generator::~Map_generator(){
	printf("Mapa Destruido!\n");
}

void Map_generator::set_n_lines(int n_lines){
	this -> n_lines = n_lines;  
}	
int Map_generator::get_n_lines(){
	return n_lines;
}

void Map_generator::set_n_rows(int n_rows){
	this -> n_rows = n_rows; 
}

int Map_generator::get_n_rows(){
	return n_rows;
}


char** Map_generator::gera_mapa(int n_lines ,int n_rows ,char **MAPA){
	fp = fopen("mapa.txt" , "w"); 
    MAPA = (char **) malloc(n_lines*sizeof(char*)); 
	for(int i = 0 ; i < n_lines ; i++){  
		*(MAPA+i)  = (char *) malloc(n_rows*sizeof(char)); 
	}

	//COLOCA AS LETRAS ACIMA DA MATRIZ
	for(int l = 0 ; l <= n_rows ; l++){ 
		if(l%4 == 0 && l != 0)
			fprintf(fp,"%c",65+row_indicator++);  
		else
			fprintf(fp," "); 
        if(l == n_rows-1)
            fprintf(fp," "); 
	}
	fprintf(fp,"\n"); 


	//PREEENCHENDO COM CARACTERES A MINHA MATRIZ.
	for(int i = 0 ; i < n_lines; i++){
			 
		if(i != 0)
			fprintf(fp,"\n"); 
		if(i%2 != 0)
	      		fprintf(fp,"%.02d",line_indicator++); //PREENCHENCO A POSICAO DA COLUNA DA MATRIZ 
		else
			fprintf(fp,"  ");  
		
        for(int j = 0 ; j < n_rows ; j++){

			if(i%2 == 0 && j%4 == 0)
				*(*(MAPA+i)+j) = '+'; 	
			
			if(i%2 == 0 && j%4 != 0)
				*(*(MAPA+i)+j) = '-';
			
			if(i%2 != 0 && j%4 == 0)
				*(*(MAPA+i)+j) = '|';
			
			if(i%2 != 0 && j%4 != 0)
				*(*(MAPA+i)+j) = ' ';
			
			
			fprintf(fp,"%c",*(*(MAPA+i)+j)); 
		}	
		
	}

	fprintf(fp,"\n");  
	fclose (fp); 

	return MAPA; 
}

char **Map_generator::gera_mapa_chars(int n_lines ,int n_rows){ //Lembrar de converter os numeros p/ normal
    char **MAPA_CH;
    MAPA_CH = (char **)malloc (n_lines*sizeof(char *)); 
    for(int i = 0 ; i < n_rows ; i++) 
        *(MAPA_CH+i) = (char *) malloc(n_rows*sizeof(char));
    
    for(int i = 0 ; i < n_lines ; i++){
        for(int j = 0 ; j < n_rows ; j++){
            MAPA_CH[i][j] = 'A';
        }
    }
    
    return MAPA_CH; 
}

int **Map_generator::gera_mapa_integer(int n_lines ,int n_rows){ //Lembrar de converter os numeros p/ normal
    int **gera_mapa_integer;
    int **MAPA_INT;
    MAPA_INT = (int**)malloc(n_lines*sizeof(int*)); 
    for(int i = 0 ; i < n_rows ; i++) 
        *(MAPA_INT+i) = (int *) malloc(n_rows*sizeof(int));
    
    for(int i = 0 ; i < n_lines ; i++){
        for(int j = 0 ; j < n_rows ; j++){
            MAPA_INT[i][j] = 0;
        }
    }
    return MAPA_INT;
}


char **Map_generator::carrega_mapa(int n_lines,int n_rows){
    
    FILE *fpmg; //ponteiro do tipo arquivo pra map genrator
    char ch;
   
    n_lines = n_lines+1; //n_lines*2+2
    n_rows = n_rows+3;   //n_rows*4+5
    
    char **Matriz_mapa = new char*[n_lines];
    for(int i = 0 ; i < n_rows ; i++ )
    Matriz_mapa[i] = new char[n_rows];   
    
    fpmg = fopen("mapa.txt", "r"); 	

    int iMapa = 0,
        jMapa = 0; 

    for(int i = 0; !feof(fpmg); i++){
          fscanf(fpmg,"%c", &ch);
	  if(ch == '\n' && i != 0){ iMapa++;  jMapa=0;}
	  Matriz_mapa[iMapa][jMapa] = ch;
	  jMapa++;   
    } 
  //////////////////////////////////////////  
    for(int i = 0 ; i < n_lines  ; i++){
        for(int j = 0 ;j < n_rows; j++){
            printf("%c" ,Matriz_mapa[i][j]);
        }
    }
    ///////////////////////////////////
    
    puts("");
    return Matriz_mapa;

    return 0; 
}
