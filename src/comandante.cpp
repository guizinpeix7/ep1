#include <iostream>
#include <string> 
#include "./../obj/comandante.hpp"
#include "./../obj/barco.hpp"
#include "./../obj/submarino.hpp"
#include "./../obj/canoa.hpp"
#include "./../obj/porta_avioes.hpp"
#include "./../obj/map_generator.hpp"
#include <stdlib.h>
#include <time.h>
using namespace std;

Comandante::Comandante(){
    
    
}

Comandante::~Comandante(){
    
}
int **Comandante::guarda_valores_mINT(int **matriz_int_ptr){
    return matriz_int_ptr; 
}
char **Comandante::guarda_valores_mCH(char **matriz_char_ptr){
    return matriz_char_ptr; 
}
char **Comandante::guarda_valores_mM(char **matriz_mapa_ptr){
    return matriz_mapa_ptr; 
}


int Comandante::QGmain(int n_lines,int n_rows){
    char **ch_map; 
    int **int_map; 
	char **matriz_mapa; 
    
    Map_generator mapa1;
    int n_barcos = 12; 
    mapa1.set_n_lines(n_lines*2+1);	
	mapa1.set_n_rows(n_rows*4+1);
	
    mapa1.carrega_mapa(mapa1.n_lines,mapa1.n_rows); 
    
	matriz_mapa = mapa1.gera_mapa(mapa1.n_lines,mapa1.n_rows,mapa1.MAPA);

    ch_map = mapa1.gera_mapa_chars(n_lines,n_rows);

    int_map = mapa1.gera_mapa_integer(n_lines,n_rows);

    instancia_barcos(matriz_mapa, ch_map,int_map, n_lines,n_rows);
    
    return 0;     
}

int Comandante::instancia_barcos(char **MAPA, char **MAPA_CH , int **MAPA_INT,int n_lines ,int n_rows){
    FILE *fps;
    fps = fopen("map_1.txt", "r");
    char lixo; 
    char t_barco[25];
    char sentido[25];
    char c = '\0'; 
    int p_x = 0 , p_y = 0; 
    int pontos_totais=0;
    Canoa *canoa = new Canoa[10] ;
    Submarino *submarino = new Submarino[10];
    Porta_avioes *porta_avioes = new Porta_avioes[10];
    cout << "n_lines =" << n_lines << endl <<endl; 
    
    while(!feof(fps)){
        
        fscanf(fps,"%d",&p_x); //cout << p_x  << " "; //Numero 
        
        lixo = fgetc(fps); //cout << lixo; //Espaco
        
        fscanf(fps,"%d",&p_y);// cout << p_y << " "; // Numero
        
        lixo = fgetc(fps); //cout << lixo; //Espaco
        
        fscanf(fps,"%s",t_barco);  
    //    cout << t_barco <<  " "; 
        fgetc(fps); 
        
        fscanf(fps,"%s",sentido);  
  //      cout << sentido <<" " ; 
        fgetc(fps); 
        
//        cout << endl << endl;
        
        
        //Tambem pode ser usado o strcmp
    if(t_barco[0] == 'c' || t_barco[0] == 'C'){
        
            canoa -> posicoes(p_y,p_x, MAPA_CH);
            canoa -> vida(p_y,p_x, MAPA_INT);
            pontos_totais ++; 
        }
    
        
        if(t_barco[0] == 's' || t_barco[0] == 'S')
        { 
            submarino -> posicoes(p_y,p_x,sentido,MAPA_CH);            
            submarino -> vida(p_y,p_x,sentido, MAPA_INT);
            pontos_totais +=4; 
        }
        
        if(t_barco[0] == 'p' || t_barco[0] == 'P'){
            porta_avioes -> posicoes(p_y,p_x,sentido,MAPA_CH); 
            porta_avioes -> vida(p_y,p_x,sentido, MAPA_INT);
            pontos_totais+=4; 
        }
            
    }

    
    cout << "n_lines =" << n_lines << endl <<endl; 
    for(int i = 0 ;  i < n_lines-1 ; i++){
        for(int j = 0; j < n_rows-1 ; j++){
            printf("%d ", MAPA_INT[i][j]);
        }
        cout << endl; 
    }
    
    Show_game(MAPA, MAPA_CH , MAPA_INT,n_lines ,n_rows,pontos_totais); 
       
}

int Comandante::Show_game(char **MAPA, char **MAPA_CH , int **MAPA_INT,int n_lines ,int n_rows,int pontos_totais){
        int meus_pontos= 0; 
        while(meus_pontos <= pontos_totais){
            int a,b; 
            cout << "Insira uma linha entre 0 e " << n_lines-1 <<":"<<endl ;  
            scanf("%d", &a);
            cout << "Insira uma coluna entre 0 e "<< n_rows-1<< ":" <<endl; 
            scanf("%d", &b);
            
            if(a < n_lines || b < n_rows){
                MAPA[a*2+1][b*4+2] = MAPA_CH[a][b];
                
                for(int i = 0 ; i < n_lines*2+1; i++){
                    for(int j = 0 ; j < n_rows*4+2 ; j++){
                        printf("%c ", MAPA[i][j]);
                    }
                    printf("\n");
                }
                if(MAPA_CH[a][b] == 'C'){
                        cout << "Voce acertou uma canoa!"<<endl;
                        meus_pontos++; 
                }
                if(MAPA_CH[a][b] == 'S'){
                    cout << "Voce acertou um submarino!... glup!glup!"<<endl;
                    meus_pontos++; 
                }
                if(MAPA_CH[a][b] == 'P'){
                    cout << "Voce acertou um Porta-avioes!... glup!glup!"<<endl;
                    meus_pontos++; 
                }
                cout << "Seus Pontos: " <<"["<<meus_pontos<<"]"<< endl;
            }
            else{
                cout<<"Tente novamente, mas dentro dos limites da guerra!"<<endl; 
            }
            
        }
}
