#ifndef COMANDANTE_HPP
#define COMANDANTE_HPP
#include <string>
#include <iostream>

class Comandante{
private: 

public: 
    Comandante();
    ~Comandante();
    int instancia_barcos(char** , char**, int**,int,int);
    int QGmain(int ,int);
    int **guarda_valores_mINT(int **matriz_int_ptr);
    char **guarda_valores_mCH(char **matriz_char_ptr);
    char **guarda_valores_mM(char **matriz_mapa_ptr);

    int Show_game(char **MAPA, char **MAPA_CH , int **MAPA_INT,int n_lines ,int n_rows,int pontos_totais);
};


#endif
