#ifndef PORTA_AVIOES_HPP
#define PORTA_AVIOES_HPP

#include <iostream>
#include <string>
#include "barco.hpp"

using namespace std; 

class Porta_avioes: public Barco{
private: 

public: 
	Porta_avioes(); 
	~Porta_avioes(); 
	int habilidade_especial(); 
    int posicoes(int,int,char*, char**); 
    int vida(int p_y, int p_x,char *sentido,int **MAPA_INT);
    
};


#endif
