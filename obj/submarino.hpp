#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include <iostream>
#include <string.h>
#include "barco.hpp"

using namespace std; 

class Submarino : public Barco{
private: 
public:
	Submarino(); 
    ~Submarino(); 
	
    int habilidade_especial();
    
    int posicoes(int,int,char*, char**); 
    int vida(int p_y, int p_x,char *sentido,int **MAPA_INT);

};

#endif
