#ifndef MAP_GENERATOR_HPP
#define MAP_GENERATOR_HPP

#include <iostream>
#include <string> 

using namespace std; 

class Map_generator{

public:
	char **MAPA; 
	int line_indicator; 
	int row_indicator;
	int n_lines; 
	int n_rows;

	Map_generator(); 
	~Map_generator(); 	
 
	FILE *fp; 

	int get_n_lines();
	void set_n_lines(int n_lines); 

	int get_n_rows();
       	void set_n_rows(int n_rows); 	

	char **gera_mapa(int,int,char**); 
	int zera_valores(); 
    char **carrega_mapa(int,int);
    char **gera_mapa_chars(int ,int );
    int **gera_mapa_integer(int ,int);
    int instancia_barcos(char **, char **, int **, int ,int);
    

    

};

#endif
