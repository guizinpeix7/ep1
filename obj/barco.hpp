#ifndef BARCO_HPP
#define BARCO_HPP

#include <iostream>
#include <string> 

using namespace std; 

class Barco{
private: 
	string orientacao;  
    string tipo_de_barco; 
    int tamanho; 
	int localizacao_x;
	int localizacao_y;
	int vida_total; 	
    int vida_atual;

public:
	Barco();
	~Barco(); 
    
    string get_orientacao(); 
    void set_orientacao(string orientacao); 
    
    string get_tipo_de_barco(); 
    void set_tipo_de_barco(string tipo_de_barco); 
    
    int get_tamanho();
	void set_tamanho(int tamanho); 
    
    int get_localizacao_x(); 
    void set_localizacao_x(int localizacao_x);
    
    int get_localizacao_y(); 
    void set_localizacao_y(int localizacao_y);
    
    int get_vida_total();
    void set_vida_total(int vida_total);
    
    int get_vida_atual();
    void set_vida_atual(int vida_atual);
    
    int habilidade_especial();
    
    int posicoes(int ,int ,char **); 
    
    //int posicoes(int ,int ,char ,char **); 
    
    int afundar(int vida);
    
}; 

#endif
