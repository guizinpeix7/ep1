#ifndef CANOA_HPP
#define CANOA_HPP
#include <iostream>
#include <string>
#include "barco.hpp"

using namespace std; 

class Canoa : public Barco{
private:    
public:
	Canoa();
	~Canoa(); 
    
    int posicoes(int p_y ,int p_x,char **MAPA_CH);
   int vida(int p_y ,int p_x,int **MAPA_INT);
};


#endif
