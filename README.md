# EP1 - OO 2019.1 (UnB - Gama)

&nbsp;&nbsp;Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval), onde todas suas instruções, dicas e requisitos para a avaliação estão sendo descritos na [wiki](https://gitlab.com/oofga/eps/eps_2019_1/ep1/wikis/home) do repositório.

## Instruções

&nbsp;&nbsp;Recomenda-se construir o corpo do README com no mínimo no seguinte formato:

* Descrição do projeto
O projeto foi pensando utilizando 3 matrizes, sendo uma de caracteres indicando qual tipo de barco se encontrava em determinada posicao, uma contendo a vida dos barcos e a ultima para exibir os dados no terminal de forma elegante. 
Infelizmente ao colocar os arquivos no make, o programa comecou a presentar erro de segmentation fault na leituda do arquivo, que inseriria as instrucoes.
Alem desse problema so consegui preencher e executar com sucesso a leitura de uma matriz, mas essa felizmente conseguiu atender todos os requisitos, de colisao, de exibicao de placar e atualizacao, tudo executado com excelencia.
Apos erros cometidos devido ao sono que tive as 5h, no qual apaguei todos os objetos criados, desisti de tentar corrigir o erro de segmentation fault, e fui na busca implacavel de reorganizar os objetos apagados.   
Alem disso o programa em algumas partes foi pensado para ser utilizado dinamicamente. De forma que o jogador poderia escolher a dimencao do mapa. O que falhou na utilizacao do make. 
* Instruções de execução
A execucao pode ser feita utilizando o make run, porem este ira apresentar segmentation fault

&nbsp;&nbsp;De forma básica, a construção e execução do projeto funciona da seguinte maneira:

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

Caso já tenha, posteriormente a uma mudança, criado os objetos definidos, lembre-se sempre de criar o objeto dos novos, limpando os antigos:
```
$ make clean
```

## Observações

* Fique avontade para manter o README da forma que achar melhor, mas é importante manter ao menos as informações descritas acima para, durante a correção, facilitar a avaliação.
* Divirtam-se mas sempre procurando atender aos critérios de avaliação informados na Wiki, ela definirá a nota a ser atribuída ao projeto.
